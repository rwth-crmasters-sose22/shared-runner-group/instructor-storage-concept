---
title: Tracking Concept Project DDP
description: Use passive tracking of Bluetooth devices and Beacons to enhance construction
author: 
keywords: marp,marp-cli,slide
url: 
marp: true
image: 
---

# Storage optimization

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence {
    display: flex;
    justify-content: center;
    align-items: center;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: 45%;
    max-height: 45%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories

- _"For a whole 4 months I walked above metal rebars that was laying infront of my office door in the construction site"_, said :construction_worker: on the :building_construction:
- _"The storage area was completely filled up with cement bags, so we decided to distribute the excess cement bags into a site station where it should be used after 2-3 days, however during those days cement bags was damaged, corrupted and even there were missing bags"_, said :construction_worker: on the :building_construction:
- _"The workforce is using the paint buckets to work on them , as the buckets is scattered all over the place"_, said :construction_worker: on the :building_construction:

---

# Mindmap

::: fence

@startuml

@startmindmap

*[#Orange] Construction storage
** Time and circumstance/condition
*** Building material
**** Storage specs
**** Ready-on-demand
***** Raw material
***** Special orders
*** Arrival
**** Actual arrival
**** Planned arrival
*** Quantity
**** Planned to be used
**** Actual needed
*** Quality
**** Vendor list
**** Constuctor supplier
*** Purchase
**** Tender
**** BOQ
**** Actual pricing
** Place
*** Construction zone
**** Fixed machinery
**** Cranes
**** Building zone
*** Storage zone
**** Scaffolding
**** Enclosed Storage
**** Wastes
*** Routes
**** Labor flow
**** Driven Equipment
*** Temparory structure
**** Caravans
**** Enclosed storage
*** Access point
**** Supply off load bay
**** Entrance/Exit


@endmindmap

@enduml

:::

---

# Open-source & hardware secondary research

* [GreaterWMS](https://github.com/Singosgu/GreaterWMS) Open source warehouse management system. API uses restful protocol to facilitate for add-on functions development.
<!--* [modern-data-warehouse-dataops](https://github.com/Azure-Samples/)-->
* [WDA-Main](https://github.com/shucheng-ai/WDA-main) An AI tool to generate warehouse layout design in 2D and 3D format. WDA can interpret warehouse structures from CAD drawings, and generate layout designs of inventory.
* [RackLay](https://github.com/Avinash2468/RackLay) Monocular Multi-Layered Layout Estimation for Warehouses with Sim2Real Transfer
* [js-Simulator](https://github.com/chen0040/js-simulator) Is a general-purpose discrete-event multiagent simulator for agent-based modelling and simulation. Agent-Based Modelling and Simulation. It was written entirely in Javascript.


---

# Locked concept

...
